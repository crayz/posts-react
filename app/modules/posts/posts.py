from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from bson import json_util
from bson.json_util import dumps
from bson.objectid import ObjectId
from datetime import datetime
import json

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    post = g.postsdb.getPosts(session['username'])
    posts = list(post)
    array = dumps(posts,default=json_util.default)
    count = 0
    newpost=[]
    for post in posts:
        rec = dumps(post,default=json_util.default)
        json1_data = json.loads(rec)
        objectid = json1_data['_id']
        oidstring = objectid['$oid']
        x = {'post' : json1_data["post"] ,'emotion':json1_data["emotion"],'OID':oidstring}
        newpost.insert(count,x)
        count += 1  
    print "\n"
    print newpost[0]

    return render_template('posts/post.html', posts=posts, posts2=newpost, date=datetime.utcnow().strftime('%B %d %Y'))

@mod.route('/delete', methods=['POST'])
def delete_post():
    print request.form['post_to_delete']
    g.postsdb.deletePost(request.form['post_to_delete'])
    redirect(url_for('.post_list'))
    flash('Post Deleted!', 'remove_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    emotion = request.form['emotions']
    g.postsdb.createPost(new_post, session['username'], emotion)
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))